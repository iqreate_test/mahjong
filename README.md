# mahjong

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
1. Understanding the system of scoring used

This game is not a single standardized one, like that of ‘western’ chess that is Karpov style, but it exists in more than two dozen regional and national variants. The fundamentals of this game are to draw and discard and there is an objective to form a pair and four groups, which is common for all. It is the different scoring system used that makes the main difference.

Learning how a finished Mahjong hand is to be appropriately scored, the values of the points, needs practice. No mistake is to be made and one should learn by the hard way about the different scoring elements present in the variants of the game, for becoming a consistent winner. Skill here is necessary and it is the capability of selecting quickly the possible scoring elements and their combinations, which suit the tiles better and could assist in achieving the minimum point score. Also, it is essential to perform it under pressure, while adapting flexibly to the constantly changing game. While drawing new tiles, the opponents would have their discards thrown out to develop their hands. Thus new opportunities arise as the earlier ones disappear. Understanding the scoring system that is in use cannot substitute anything else, including the capability to apply constantly to the development of the hand. Only then can the winning abilities be developed in this game.

2. Assessing the Hand

Once the starting tile is picked up, the very first thing to do is to have the direction and strength of the hand assessed properly. When the winning hand can be assembled in variety of ways from the different scoring elements, it becomes clear why.

A challenge is offered by this great Mahjong game to the players to have their play balanced between aiming for going out quickly, however, without the highly valuable tile sets in hand or to shoot for the harder-to-achieve combinations that are higher paying. Hence, within seconds, you need to assess the best possible options, while playing serious stakes for placing a winning hand together. Is honour tiles discarded or kept? Do you regarded an all-pong hand or all chow based hands to be the best bet or something that is much more exotic?

Assessing the hand is rather an art and a continuous process with the developing of your game. Other options are to be kept open and to switch the play as dictated or when there are new opportunities. Undoubtedly, flexibility is a must.

3. Understanding the Odds

For improving the hand, the outs are to be kept track of, similar to poker. It is the available tiles that can help to improve the hand and the odds to acquire them are to be evaluated properly. When there is a choice to discard any tile, it is necessary to discard the one, which would have the mathematical winning odds to be maximized with remaining tiles present in hand.

4. Watch the Discards

Mahjong does not require many more skills to be known than the capability of tracking the tiles your opponent chose to discard. It would specify the kind of hands they have been planning to develop and compel you to play your own according to circumstances. Your opponents are sure to watch you similarly.

What are the tiles that are safe to be discarded? In case, West Wind is thrown up by the opponent, you can also follow suit. But, you need to know if your right hand opponent has discarded any Winds or Dragons. Discarding a tile as such can prove to be fatal, since the opponent may seize the opportunity immediately. Is the left hand opponent having all Bamboo tiles discarded? In such a case, collecting these can help you to make him pay. In different Mahjong variants, the finale tile that is discarded by a player, especially when an opponent needs it the most for going out, is punished by having to pay additional points for being careless. It needs to be avoided.

5. To Meld / Not Meld

A tile which is discarded by an opponent is not to be claimed automatically just because it help in completing (meld) a group in the hand. Reasons that are cited are if such discards are claimed and group completed, then this group is to be exposed to all. This way, the opponents are likely to know more about the hand from the discards and melds, than what they might have from the discards alone. Each time, the player melds, it tends to narrow the hand’s remaining opportunity. Another reason is with more winning hands being concealed, the points can be scored. Therefore, you need to have solid reason for claiming discarded tiles.

6. Defensive play

It could be that even if you badly want to put together the winning hand, there are times, especially towards the hand’s end, when it can be a much wiser tactic to prevent the opponent from winning instead. If available tiles for drawing are exhausted by the players, then the round finishes, without any person making a score, while a draw triggers a loss, each time.

It is for this reason, towards the end, if you realize that the odds are packed very heaving against your game, then switching to defensive mode would be advisable, if from the discards and melds, you are stacked against high scoring hand. For preventing the opponent player from going out, it may be necessary to break up groups ruthlessly in the hand and to have tiles judged to be safe discarded. Reverse strategy, when smartly taken can help in saving a good amount of money.
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
