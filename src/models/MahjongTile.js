export class MahjongTile {
  constructor(
    type,
    name,
    value
  ) {
    this.type = type;
    this.name = name;
    this.value = value;
  }

  get isYauPai(){
    let suits = ['sou', 'man', 'pin']
    if(this.type === 'dragon'){
      return true;
    } else if(this.type === 'wind'){
      return true;
    } else if(suits.indexOf(this.type) != -1 && (this.value == 1 || this.value == 9) ){
      return true;
    }
  }
}
